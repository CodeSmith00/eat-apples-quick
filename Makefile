SMOLPXL_JS_VERSION := v0.5.3
DOWNLOAD_URL := https://gitlab.com/smolpxl/smolpxl/-/raw

all: test

compile:
	cargo fmt
	wasm-pack build

test: compile
	cargo test
	wasm-pack test --firefox --headless

run: compile
	cd www && npm install && npm run start

dist: test
	cd www && npm install && npm run-script build

download-smolpxl-js:
	curl --silent --output www/smolpxl.js \
		"${DOWNLOAD_URL}/${SMOLPXL_JS_VERSION}/public/smolpxl.js"
	curl --silent --output www/smolpxl.css \
		"${DOWNLOAD_URL}/${SMOLPXL_JS_VERSION}/public/smolpxl.css"
	# Patch smolpxl.js to be a module
	echo "export default Smolpxl;" >> www/smolpxl.js
