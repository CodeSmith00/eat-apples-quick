#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Item {
    Apple,
    Pill,
    Space,
    Wall,
}

impl Item {
    pub fn from(ch: char) -> Option<Item> {
        match ch {
            '-' => Some(Item::Apple),
            '=' => Some(Item::Pill),
            '.' => Some(Item::Space),
            '#' => Some(Item::Wall),
            _ => None,
        }
    }

    #[allow(dead_code)]
    pub fn to_char(&self) -> char {
        match self {
            Item::Apple => '-',
            Item::Pill => '=',
            Item::Space => '.',
            Item::Wall => '#',
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn items_may_be_encoded_and_decoded() {
        assert_eq!(Some(Item::Apple), Item::from(Item::Apple.to_char()));
        assert_eq!(Some(Item::Pill), Item::from(Item::Pill.to_char()));
        assert_eq!(Some(Item::Space), Item::from(Item::Space.to_char()));
        assert_eq!(Some(Item::Wall), Item::from(Item::Wall.to_char()));
    }

    #[test]
    fn nonitem_chars_decode_to_none() {
        assert_eq!(None, Item::from('*'));
    }
}
