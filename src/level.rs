use std::convert::TryFrom;

use crate::ghost::Ghost;
use crate::item::Item;
use crate::level_parser::{level_to_string, string_to_level};
use crate::player::Player;

#[derive(Clone)]
pub struct Level {
    pub name: String,
    pub grid: Vec<Vec<Item>>,
    pub player: Player,
    pub ghosts: Vec<Ghost>,
}

impl Level {
    pub fn from(lines: &str) -> Level {
        string_to_level(lines)
    }

    pub fn to_string(&self) -> String {
        level_to_string(self)
    }

    pub fn item_at(&self, x: i32, y: i32) -> Item {
        self.item_at_or_bad_coord(x, y).unwrap_or(Item::Space)
    }

    fn item_at_or_bad_coord(&self, x: i32, y: i32) -> Result<Item, ()> {
        let x = usize::try_from(x).map_err(|_| ())?;
        let y = usize::try_from(y).map_err(|_| ())?;
        self.grid
            .get(y)
            .ok_or(())
            .and_then(|row| row.get(x).ok_or(()))
            .map(|i| i.clone())
    }

    pub fn set_at(&mut self, x: i32, y: i32, item: Item) {
        let _ = self.set_at_internal(x, y, item);
    }

    pub fn width(&self) -> usize {
        self.grid[0].len()
    }

    pub fn height(&self) -> usize {
        self.grid.len()
    }

    fn set_at_internal(
        &mut self,
        x: i32,
        y: i32,
        item: Item,
    ) -> Result<(), ()> {
        let x = usize::try_from(x).map_err(|_| ())?;
        let y = usize::try_from(y).map_err(|_| ())?;
        self.grid
            .get_mut(y)
            .map(|row| {
                if let Some(entry) = row.get_mut(x) {
                    *entry = item;
                }
            })
            .ok_or(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::direction::Direction;

    fn trim_lines(s: &str) -> String {
        s.trim()
            .lines()
            .map(str::trim)
            .collect::<Vec<&str>>()
            .join("\n")
    }

    #[test]
    fn levels_may_be_encoded_and_decoded() {
        let lev = r#"
            ####
            #.>#
            -12=
            #*##
            :name=My level
            :*=-1
        "#;

        assert_eq!(Level::from(&lev).to_string(), trim_lines(lev));
    }

    #[test]
    fn level_width_and_height_are_reported() {
        let level = Level::from(
            r#"
            >#
        "#,
        );

        assert_eq!(level.width(), 2);
        assert_eq!(level.height(), 1);
    }

    #[test]
    fn player_position_is_determined_by_its_character() {
        assert_eq!(Level::from("....\n..>.").player.position, (2, 1));
        assert_eq!(Level::from("....\n..<.").player.position, (2, 1));
        assert_eq!(Level::from("....\n..^.").player.position, (2, 1));
        assert_eq!(Level::from("....\n..v.").player.position, (2, 1));
    }

    #[test]
    fn player_direction_is_determined_by_its_character() {
        assert_eq!(Level::from(">").player.direction, Direction::Right);
        assert_eq!(Level::from("<").player.direction, Direction::Left);
        assert_eq!(Level::from("^").player.direction, Direction::Up);
        assert_eq!(Level::from("v").player.direction, Direction::Down);
    }

    #[test]
    fn multiple_items_encode_to_star() {
        let lev = r#"
            **
            :*=-v
            :*=-12
        "#;
        let level = Level::from(lev);
        assert_eq!(level.player.position, (0, 0));
        assert_eq!(level.player.direction, Direction::Down);
        assert_eq!(level.ghosts[0].position, (1, 0));
        assert_eq!(level.ghosts[1].position, (1, 0));
        assert_eq!(level.to_string(), trim_lines(lev));
    }
}
