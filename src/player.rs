use crate::direction::Direction;

#[derive(Clone, Debug)]
pub struct Player {
    pub position: (i32, i32),
    pub direction: Direction,
}

impl Player {
    pub fn new(x: i32, y: i32, direction: Direction) -> Player {
        Player {
            position: (x, y),
            direction,
        }
    }

    pub fn from(x: i32, y: i32, ch: char) -> Option<Player> {
        match ch {
            '^' => Some(Player::new(x, y, Direction::Up)),
            'v' => Some(Player::new(x, y, Direction::Down)),
            '<' => Some(Player::new(x, y, Direction::Left)),
            '>' => Some(Player::new(x, y, Direction::Right)),
            _ => None,
        }
    }

    pub fn to_char(&self) -> char {
        match self.direction {
            Direction::Up => '^',
            Direction::Down => 'v',
            Direction::Left => '<',
            Direction::Right => '>',
        }
    }
}
