use itertools::Itertools;

use crate::images::Image;

pub struct Screen<'a> {
    pixels: &'a mut [u8],
    width: usize,
    height: usize,
}

impl Screen<'_> {
    pub fn new(pixels: &mut [u8], width: usize, height: usize) -> Screen {
        assert!(pixels.len() % 4 == 0);
        Screen {
            pixels,
            width,
            height,
        }
    }

    pub fn set_all(&mut self, r: u8, g: u8, b: u8) {
        let mut it = self.pixels.iter_mut();
        loop {
            if let Some(px) = it.next() {
                *px = r;
            } else {
                break;
            }
            if let Some(px) = it.next() {
                *px = g;
            } else {
                break;
            }
            if let Some(px) = it.next() {
                *px = b;
            } else {
                break;
            }
            it.next();
        }
    }

    pub fn draw(&mut self, x: i32, y: i32, image: &Image) {
        // Consider using image::imageops::overlay, instead of doing
        // this ourselves.

        assert!(image.bytes.len() % 4 == 0);

        if x >= self.width as i32
            || y >= self.height as i32
            || (x + image.width as i32) <= 0
            || (y + image.height as i32) <= 0
        {
            return;
        }

        let mut ix = 0;
        let mut iy = 0;
        for mut chunk in &image.bytes.iter().chunks(4) {
            let rx = x + ix;
            let ry = y + iy;
            if rx >= 0
                && rx < self.width as i32
                && ry >= 0
                && ry < self.height as i32
            {
                let i = (4 * (ry * self.width as i32 + rx)) as usize;
                let r = *chunk.next().unwrap();
                let g = *chunk.next().unwrap();
                let b = *chunk.next().unwrap();
                let a = *chunk.next().unwrap();
                if a == 255 {
                    self.pixels[i] = r;
                    self.pixels[i + 1] = g;
                    self.pixels[i + 2] = b;
                } else if a != 0 {
                    panic!("Image contains semi-transparent pixels!");
                }
            }
            ix += 1;
            if ix >= image.width as i32 {
                ix = 0;
                iy += 1;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    const SCREEN4X4: [[u8; 4]; 16] = [
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        //
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        //
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        //
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        [0, 0, 0, 255],
        [0, 0, 0, 255],
    ];

    fn flat_vec(pixels: &[[u8; 4]]) -> Vec<u8> {
        pixels.iter().flatten().copied().collect()
    }

    #[test]
    fn set_all_changes_all_pixels() {
        let mut pixels: Vec<u8> = flat_vec(&SCREEN4X4);
        {
            let mut screen = Screen::new(pixels.as_mut_slice(), 4, 4);
            screen.set_all(3, 6, 9);
        }
        assert_eq!(
            pixels,
            flat_vec(&[
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                //
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                //
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                //
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                [3, 6, 9, 255],
                [3, 6, 9, 255],
            ])
        );
    }

    #[test]
    fn image_drawn_at_top_left() {
        let mut pixels: Vec<u8> = flat_vec(&SCREEN4X4);
        let image = Image {
            width: 2,
            height: 3,
            bytes: flat_vec(&[
                [100, 101, 102, 255],
                [110, 111, 112, 255],
                //
                [103, 104, 105, 255],
                [113, 114, 115, 255],
                //
                [106, 107, 108, 255],
                [116, 117, 118, 255],
            ]),
        };

        {
            let mut screen = Screen::new(pixels.as_mut_slice(), 4, 4);
            screen.draw(0, 0, &image);
        }

        assert_eq!(
            pixels,
            flat_vec(&[
                [100, 101, 102, 255],
                [110, 111, 112, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [103, 104, 105, 255],
                [113, 114, 115, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [106, 107, 108, 255],
                [116, 117, 118, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
            ])
        );
    }

    #[test]
    fn image_drawn_within_screen() {
        let mut pixels: Vec<u8> = flat_vec(&SCREEN4X4);

        let image = Image {
            width: 3,
            height: 2,
            bytes: flat_vec(&[
                [100, 101, 102, 255],
                [110, 111, 112, 255],
                [120, 121, 122, 255],
                //
                [103, 104, 105, 255],
                [113, 114, 115, 255],
                [123, 124, 125, 255],
            ]),
        };

        {
            let mut screen = Screen::new(pixels.as_mut_slice(), 4, 4);
            screen.draw(1, 2, &image);
        }

        assert_eq!(
            pixels,
            flat_vec(&[
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [0, 0, 0, 255],
                [100, 101, 102, 255],
                [110, 111, 112, 255],
                [120, 121, 122, 255],
                //
                [0, 0, 0, 255],
                [103, 104, 105, 255],
                [113, 114, 115, 255],
                [123, 124, 125, 255],
            ])
        );
    }

    #[test]
    fn image_drawn_overlapping_top_left() {
        let mut pixels: Vec<u8> = flat_vec(&SCREEN4X4);

        let image = Image {
            width: 2,
            height: 2,
            bytes: flat_vec(&[
                [100, 101, 102, 255],
                [110, 111, 112, 255],
                //
                [103, 104, 105, 255],
                [113, 114, 115, 255],
            ]),
        };

        {
            let mut screen = Screen::new(pixels.as_mut_slice(), 4, 4);
            screen.draw(-1, -1, &image);
        }

        assert_eq!(
            pixels,
            flat_vec(&[
                [113, 114, 115, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
            ])
        );
    }

    #[test]
    fn image_drawn_overlapping_top_right() {
        let mut pixels: Vec<u8> = flat_vec(&SCREEN4X4);

        let image = Image {
            width: 3,
            height: 3,
            bytes: flat_vec(&[
                [100, 101, 102, 255],
                [110, 111, 112, 255],
                [120, 121, 122, 255],
                //
                [103, 104, 105, 255],
                [113, 114, 115, 255],
                [123, 124, 125, 255],
                //
                [106, 107, 108, 255],
                [116, 117, 118, 255],
                [126, 127, 128, 255],
            ]),
        };

        {
            let mut screen = Screen::new(pixels.as_mut_slice(), 4, 4);
            screen.draw(2, -1, &image);
        }

        assert_eq!(
            pixels,
            flat_vec(&[
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [103, 104, 105, 255],
                [113, 114, 115, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [106, 107, 108, 255],
                [116, 117, 118, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
            ])
        );
    }

    #[test]
    fn image_drawn_overlapping_bottom_right() {
        let mut pixels: Vec<u8> = flat_vec(&SCREEN4X4);

        let image = Image {
            width: 3,
            height: 3,
            bytes: flat_vec(&[
                [100, 101, 102, 255],
                [110, 111, 112, 255],
                [120, 121, 122, 255],
                //
                [103, 104, 105, 255],
                [113, 114, 115, 255],
                [123, 124, 125, 255],
                //
                [106, 107, 108, 255],
                [116, 117, 118, 255],
                [126, 127, 128, 255],
            ]),
        };

        {
            let mut screen = Screen::new(pixels.as_mut_slice(), 4, 4);
            screen.draw(3, 2, &image);
        }

        assert_eq!(
            pixels,
            flat_vec(&[
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [100, 101, 102, 255],
                //
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [0, 0, 0, 255],
                [103, 104, 105, 255],
            ])
        );
    }

    #[test]
    fn image_off_screen_not_drawn() {
        let mut pixels: Vec<u8> = flat_vec(&SCREEN4X4);

        let image = Image {
            width: 2,
            height: 2,
            bytes: flat_vec(&[
                [100, 101, 102, 255],
                [110, 111, 112, 255],
                //
                [103, 104, 105, 255],
                [113, 114, 115, 255],
            ]),
        };

        {
            let mut screen = Screen::new(pixels.as_mut_slice(), 4, 4);
            screen.draw(-2, -2, &image);
            screen.draw(1, -2, &image);
            screen.draw(4, -2, &image);
            screen.draw(-2, 2, &image);
            screen.draw(4, 2, &image);
            screen.draw(-2, 4, &image);
            screen.draw(2, 4, &image);
            screen.draw(4, 4, &image);
        }

        assert_eq!(pixels, flat_vec(&SCREEN4X4));
    }
}
