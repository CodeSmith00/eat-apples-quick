use js_sys::{JsString, Object, Reflect};
use wasm_bindgen::prelude::*;
use web_sys::console;

use crate::all_levels::LEVELS;
use crate::direction::Direction;
use crate::item::Item;
use crate::model::{Model, PlayerState};
use crate::smolpxl_bindings::RunningGame;
use crate::utils::COMPLETION_WAIT_FRAMES;

const MAX_QUEUED_INPUT: usize = 3;

fn name_of_input(input: JsValue) -> Result<String, String> {
    let input = Object::try_from(&input);
    if let Some(input) = input {
        Reflect::get(input, &JsString::from("name"))
            .map(|s| s.as_string().unwrap())
            .map_err(|_| String::from("input did not have a name"))
    } else {
        Err(String::from("input was not an object"))
    }
}

fn turn(model: &mut Model, dir: Direction) {
    model.level.player.direction = dir;
    model.player_frame = 2;
    model.player_frame_dir = -1;
}

fn walk(model: &mut Model, offset: (i32, i32)) {
    let mut pos = (
        model.level.player.position.0 + offset.0,
        model.level.player.position.1 + offset.1,
    );
    let w = model.level.width() as i32;
    let h = model.level.height() as i32;
    if pos.0 < 0 {
        pos.0 = w - 1;
    } else if pos.0 >= w {
        pos.0 = 0;
    }
    if pos.1 < 0 {
        pos.1 = h - 1;
    } else if pos.1 >= h {
        pos.1 = 0;
    }

    let item = model.level.item_at(pos.0, pos.1);
    if item != Item::Wall
        || (model.pill_counter > 0 && model.ghost_at(pos.0, pos.1))
    {
        model.level.player.position.0 = pos.0;
        model.level.player.position.1 = pos.1;

        if item == Item::Apple {
            model.level.set_at(pos.0, pos.1, Item::Space);
            model.level_score += apple_score(model.level_frame);
            if !model.has_any_apples() {
                model.player_state = if model.level_num == LEVELS.len() - 1 {
                    model.level_frame = 0;
                    PlayerState::Complete
                } else {
                    PlayerState::Won
                };
            }
        } else if item == Item::Pill {
            model.level.set_at(pos.0, pos.1, Item::Space);
            model.pill_counter = 60 * 4;
            for ghost in &mut model.level.ghosts {
                ghost.dead = false;
            }
        }
    }
}

fn apple_score(tick: usize) -> usize {
    // Scaled version of sigmoid function x / sqrt(1+x^2)
    const TIME_MULT: f64 = 0.3 * (1.0 / 60.0); // Assuming 60 fps
    const MIN_SCORE: f64 = 1.0;
    const MAX_SCORE: f64 = 10.0;
    let x = (TIME_MULT * tick as f64) - 2.0;
    let sig = x / (1.0 + (x * x)).sqrt();
    let inverted_sig = 1.0 - sig;
    (MIN_SCORE + (MAX_SCORE * 0.5 * inverted_sig)).round() as usize
}

fn update_frames(model: &mut Model) {
    model.player_frame += model.player_frame_dir;
    if model.player_frame > 9 {
        model.player_frame = 9;
        model.player_frame_dir = -1;
    } else if model.player_frame < 0 {
        model.player_frame = 0;
        model.player_frame_dir = 1;
    }
}

fn read_input(running_game: &RunningGame, model: &mut Model) {
    for input in running_game.input().iter() {
        match name_of_input(input) {
            Err(e) => console::error_1(&e.into()),
            Ok(s) => match s.as_str() {
                "UP" => model.queued_input.push_front(Direction::Up),
                "DOWN" => model.queued_input.push_front(Direction::Down),
                "LEFT" => model.queued_input.push_front(Direction::Left),
                "RIGHT" => model.queued_input.push_front(Direction::Right),
                _ => (),
            },
        }
        // Forget older input if we've stored too much
        model.queued_input.truncate(MAX_QUEUED_INPUT);
    }
}

fn update_player(mut model: &mut Model) {
    match model.queued_input.pop_back() {
        Some(dir) => {
            let off = match dir {
                Direction::Up => (0, -1),
                Direction::Down => (0, 1),
                Direction::Left => (-1, 0),
                Direction::Right => (1, 0),
            };
            turn(&mut model, dir);
            walk(&mut model, off);
        }
        _ => (),
    }
}

fn update_ghosts(model: &mut Model) {
    if model.level_frame % 60 == 0 && model.pill_counter == 0 {
        let mut used_positions = vec![];
        for ghost in &mut model.level.ghosts {
            let dx = model.level.player.position.0 - ghost.position.0;
            let dy = model.level.player.position.1 - ghost.position.1;
            let mut new_pos = ghost.position.clone();
            if dx.abs() >= dy.abs() {
                new_pos.0 += dx.signum();
            } else {
                new_pos.1 += dy.signum();
            }
            if !used_positions.contains(&new_pos) {
                ghost.position = new_pos;
            }
            used_positions.push(ghost.position);
        }
    }

    if model.pill_counter > 0 {
        model.pill_counter -= 1;
        if model.pill_counter == 0 {
            for ghost in &mut model.level.ghosts {
                ghost.dead = false;
            }
        }
    }
}

fn update_collisions(model: &mut Model) {
    let x = model.level.player.position.0;
    let y = model.level.player.position.1;
    if model.pill_counter == 0 {
        if model.ghost_at(x, y) {
            if model.lives == 0 {
                model.player_state = PlayerState::Lost;
            } else {
                model.lives -= 1;
                model.player_state = PlayerState::Dead;
            }
        }
    } else {
        if model.kill_ghost_at(x, y) {
            model.level_score += 20;
            model.captures += 1;
            if model.captures > 3 {
                model.captures -= 4;
                model.lives += 1;
            }
        }
    }
}

pub fn update_model(running_game: RunningGame, mut model: &mut Model) {
    match model.player_state {
        PlayerState::Playing => {
            model.level_frame += 1;
            read_input(&running_game, &mut model);
            update_ghosts(&mut model);
            update_player(&mut model);
            update_collisions(&mut model);
            update_frames(&mut model);
        }
        PlayerState::Won => {
            if pressed_enter(&running_game) {
                model.go_to_level(model.level_num + 1);
                model.previous_score += model.level_score;
                model.level_score = 0;
            }
        }
        PlayerState::Dead => {
            if pressed_enter(&running_game) {
                model.player_state = PlayerState::Playing;
                model.go_to_level(model.level_num);
            }
        }
        PlayerState::Complete => {
            model.level_frame += 1;
            update_frames(&mut model);
            if model.level_frame > COMPLETION_WAIT_FRAMES
                && pressed_enter(&running_game)
            {
                end_game(&running_game, model);
            }
        }
        PlayerState::Lost => {
            if pressed_enter(&running_game) {
                end_game(&running_game, model);
            }
        }
    }
}

fn end_game(running_game: &RunningGame, model: &mut Model) {
    model.go_to_level(0);
    let score = model.previous_score + model.level_score;
    if score > model.high_score {
        model.high_score = score;
    }
    model.previous_score = 0;
    model.level_score = 0;
    model.lives = 3;
    model.captures = 0;
    running_game.end_game();
}

fn pressed_enter(running_game: &RunningGame) -> bool {
    running_game.received_input("SELECT") != JsValue::UNDEFINED
        || running_game.received_input("LEFT_CLICK") != JsValue::UNDEFINED
}
