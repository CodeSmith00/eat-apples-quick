use js_sys::Array;
use wasm_bindgen::JsValue;

pub const COMPLETION_WAIT_FRAMES: usize = 60;

#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

pub fn set_panic_hook() {
    // When the `console_error_panic_hook` feature is enabled, we can call the
    // `set_panic_hook` function at least once during initialization, and then
    // we will get better error messages if our code ever panics.
    //
    // For more details see
    // https://github.com/rustwasm/console_error_panic_hook#readme
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();
}

pub fn js_array(values: &[&str]) -> Array {
    values
        .into_iter()
        .map(|x| JsValue::from_str(x))
        .collect::<Array>()
}
