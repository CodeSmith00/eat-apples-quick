use core::cmp::min;

use crate::direction::Direction;
use crate::ghost::{Ghost, GhostType};
use crate::images::{
    Image, IMAGES, LIVES_PIXELS, NUM_PLAYER_FRAMES, PIXELS_PER_SQUARE,
    VICTORY_PIXELS,
};
use crate::item::Item;
use crate::model::{Model, PlayerState};
use crate::screen::Screen;
use crate::smolpxl_bindings::SmolpxlScreen;
use crate::utils::{js_array, COMPLETION_WAIT_FRAMES};

fn image_for_ghost<'a>(ghost: &Ghost) -> &'a Image {
    match ghost.ghost_type {
        GhostType::Blue => &IMAGES.ghost_blue,
        GhostType::Red => &IMAGES.ghost_red,
    }
}

fn image_for_frozen_ghost<'a>(ghost: &Ghost) -> &'a Image {
    if ghost.dead {
        &IMAGES.ghost_dead
    } else {
        &IMAGES.ghost_frozen
    }
}

fn view_grid(screen: &mut Screen, model: &Model) {
    let mut x = 0;
    let mut y = 0;
    for line in &model.level.grid {
        for item in line {
            let xp = x * PIXELS_PER_SQUARE;
            let yp = y * PIXELS_PER_SQUARE;
            match item {
                Item::Apple => screen.draw(xp, yp, &IMAGES.apple),
                Item::Pill => screen.draw(xp, yp, &IMAGES.pill),
                Item::Space => (),
                Item::Wall => screen.draw(xp, yp, &IMAGES.wall),
            }
            x += 1;
        }
        y += 1;
        x = 0;
    }
}

fn view_player(screen: &mut Screen, model: &Model) {
    let imgs = match model.level.player.direction {
        Direction::Up => &IMAGES.player.up,
        Direction::Down => &IMAGES.player.down,
        Direction::Left => &IMAGES.player.left,
        Direction::Right => &IMAGES.player.right,
    };
    screen.draw(
        model.level.player.position.0 * PIXELS_PER_SQUARE,
        model.level.player.position.1 * PIXELS_PER_SQUARE,
        &imgs[player_frame_index(model)],
    );
}

fn player_frame_index(model: &Model) -> usize {
    (NUM_PLAYER_FRAMES as f64 * model.player_frame as f64 / 10.0).floor()
        as usize
}

fn view_ghosts(screen: &mut Screen, model: &Model) {
    for ghost in &model.level.ghosts {
        let img = if model.pill_counter == 0 {
            image_for_ghost(&ghost)
        } else {
            if model.pill_counter > 60 {
                image_for_frozen_ghost(&ghost)
            } else {
                if (model.pill_counter / 10) % 2 == 0 {
                    image_for_frozen_ghost(&ghost)
                } else {
                    image_for_ghost(&ghost)
                }
            }
        };
        screen.draw(
            ghost.position.0 * PIXELS_PER_SQUARE,
            ghost.position.1 * PIXELS_PER_SQUARE,
            img,
        );
    }
}

fn view_lives(screen: &mut Screen, model: &Model) {
    let y = model.level.height() as i32 * PIXELS_PER_SQUARE;
    for x in 0..min(10, model.lives as i32) {
        screen.draw(1 + x * LIVES_PIXELS, y, &IMAGES.life);
    }
    let width = model.level.width() as i32 * PIXELS_PER_SQUARE;
    for x in 0..model.captures as i32 {
        screen.draw(width - (x + 1) * LIVES_PIXELS, y, &IMAGES.capture);
    }
}

fn view_top_messages(smolpxl_screen: &SmolpxlScreen, model: &Model) {
    smolpxl_screen.message_top_left(&format!(
        "Score: {}",
        model.previous_score + model.level_score
    ));
    smolpxl_screen
        .message_top_middle(&format!("Level {}", model.level_num + 1));
    smolpxl_screen.message_top_right(&format!("High: {}", model.high_score));
}

fn view_fullscreen_messages(smolpxl_screen: &SmolpxlScreen, model: &Model) {
    match model.player_state {
        PlayerState::Won => {
            smolpxl_screen.dim();
            smolpxl_screen.message(js_array(&[
                "",
                &format!("Level {} complete!", model.level_num + 1),
                "",
                &format!(
                    "Total score: {}",
                    model.previous_score + model.level_score
                ),
                "",
                &format!(
                    "Score for level {}: {}",
                    model.level_num + 1,
                    model.level_score
                ),
                "",
                "Press <SELECT>",
                "",
            ]));
        }
        PlayerState::Complete => {
            let score = model.previous_score + model.level_score;
            smolpxl_screen.dim();
            smolpxl_screen.message(js_array(&[
                "You finished the game!",
                "",
                &format!("Final score: {}", score),
                if score > model.high_score {
                    "New high score!"
                } else {
                    ""
                },
                "",
                &format!(
                    "Score for level {}: {}",
                    model.level_num + 1,
                    model.level_score
                ),
                "",
                if model.level_frame > COMPLETION_WAIT_FRAMES {
                    "Press <SELECT>"
                } else {
                    ""
                },
            ]));
        }
        PlayerState::Lost => {
            let score = model.previous_score + model.level_score;
            smolpxl_screen.dim();
            smolpxl_screen.message(js_array(&[
                "Game over!",
                "",
                &format!("Final score: {}", score),
                "",
                if score > model.high_score {
                    "New high score!"
                } else {
                    ""
                },
                "",
                "Press <SELECT>",
            ]));
        }
        PlayerState::Dead => {
            smolpxl_screen.dim();
            smolpxl_screen.message(js_array(&[
                "Bad luck!",
                "",
                "",
                encouraging_statement(model.lives),
                "",
                "",
                "Press <SELECT>",
            ]));
        }
        PlayerState::Playing => {}
    }
}

fn encouraging_statement(lives: usize) -> &'static str {
    match lives {
        0 => "Last life!",
        1 => "You can do this!",
        2 => "Keep trying!",
        3 => "You're doing really well!",
        _ => "Plenty of lives left!",
    }
}

pub fn view_model(
    pixels: &mut [u8],
    width: usize,
    height: usize,
    smolpxl_screen: &SmolpxlScreen,
    model: &Model,
) {
    let required_width = model.level.width() * PIXELS_PER_SQUARE as usize;
    let required_height = (model.level.height() * PIXELS_PER_SQUARE as usize)
        + LIVES_PIXELS as usize;
    if required_width != width || required_height != height {
        smolpxl_screen.set_size(required_width, required_height);
        return;
    }

    let mut screen = Screen::new(pixels, width, height);
    match model.player_state {
        PlayerState::Complete => {
            let x = ((required_width - VICTORY_PIXELS) / 2) as i32;
            let y = ((required_height - VICTORY_PIXELS) / 2) as i32;
            screen.set_all(0, 155, 0);
            screen.draw(
                x,
                y,
                &IMAGES.player.victory[player_frame_index(model)],
            );
        }
        _ => {
            view_grid(&mut screen, model);
            view_player(&mut screen, model);
            view_ghosts(&mut screen, model);
            view_lives(&mut screen, model);
        }
    }
    view_top_messages(smolpxl_screen, model);
    view_fullscreen_messages(smolpxl_screen, model);
}
