import * as wasm from "eat-apples-quick";
import TAS from "./TAS.js";
import Smolpxl from "./smolpxl.js";
import "./smolpxl.css";

TAS.patch(Smolpxl);

const game = new Smolpxl.Game();

wasm.setup_game(game);

function update(runningGame, oldModel) {
    return wasm.update(runningGame, oldModel);
}

function view(screen, model) {
    if (screen.pixels != null && model != null) {
        wasm.view(
            screen.get_pixels(),
            screen.get_width(),
            screen.get_height(),
            screen,
            model
        );
    }
}

game.start("eatapplesquick", wasm.new_model(), view, update);
